rem Make an existing set of files into a git repository and moive the content up to a remote git
rem specified as a URL and provided as an argument


rem init - Create an empty Git repository or reinitialize an existing one
git init

rem Add file contents to the index
git add .

rem commit - Record changes to the repository
rem -m Use the given <msg> as the commit message. 
git commit -m "initial commit of full repository"

rem git remote add <name> <url>  (origin is the default) 
rem Create a new connection to a remote repository. After adding a remote, you’ll be able to use 
rem as a convenient shortcut for in other Git commands.
git remote add origin %1

rem git push -u origin [branch]: Useful when pushing a new branch, this creates an upstream tracking 
rem branch with a lasting relationship to your local branch
rem git push --all: Push all branches
git push -u origin --all