# SoA #

Development package for Schedule of Activities in FHIR

## Content

On start-up the following screen is displayed.  There are 3 buttons, one to read the spreadsheet in, one to read the ODM file and one to write the loaded content to FHIR

![SoA_01](SoA_01.png)

### Loading the Spreadsheet

Once the spreadsheet has been loaded it will display as follows:

![SoA_02](SoA_02.png)

Notice that all rows and columns in the original spreadsheet are now rows in this display.

### Loading ODM

![SoA_02_ODM](SoA_02_ODM.png)

Whereas the spreadsheet only hold visits and activities, the ODM file holds visits, activities, forms and items (on a form).  In the current release it is the visits and forms that are transferred to FHIR.

### Writing to FHIR

Once either the spreadsheet or the ODM file have been loaded clicking the Write to FHIR button will format the data into FHIR resources and write it to the server - a message will pop-up when this is complete.  It may take some time.

The address of the protocol resource is shown on the FHIR tab:

![SoA_03](SoA_03.png)



### Gantt Display

Moving to the Gantt tab and clicking the Read FHIR button will load the SoA as a Gantt chart based on today's date.  This load is based on whatever is present in the Resource URL of the FHIR tab.  This allows other instances to be loaded.

#### Gantt from Spreadsheet

![SoA_04](SoA_04.png)

#### Gantt from ODM

![SoA_04_ODM](SoA_04_ODM.png)

The spreadsheet and ODM representations are for the same trial but they represent the data in different ways hence the resultant Gantt charts look different. 

# Note

The current version has the file names and the server pre-programmed.