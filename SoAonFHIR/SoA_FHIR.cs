﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;

namespace SoA_FHIR
{
    class SoA_FHIR
    {
        static public string Server = "https://brigit5.azurewebsites.net";

        static public string RetVal = "not run";

        static public FhirClient FHIRClient = null;

        static bool createFHIRClient()
        {
            if (FHIRClient == null)
            {
                FHIRClient = new FhirClient(Server);
                FHIRClient.PreferredFormat = ResourceFormat.Json;
                FHIRClient.Timeout = 120000; // The timeout is set in milliseconds, with a default of 100000

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }
            return true;
        }

        public void WriteActivity(int argID,
            string argStartDate,
            string argEndDate,
            string argName,
            int argDependendsOn)
        {
            createFHIRClient();

            ActivityDefinition activity = new ActivityDefinition();

            activity.Identifier.Add(new Identifier("bluewaveinformatics.co.uk", argID.ToString()));
            activity.Name = argName;
            {
                FhirDateTime periodStart = new FhirDateTime(argStartDate);
                FhirDateTime periodEnd = new FhirDateTime(argEndDate);
                Period period = new Period(periodStart, periodEnd);
                activity.Timing = period;
            }

        }
    }
}
