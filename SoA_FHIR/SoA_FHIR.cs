﻿//
// Sample data that has been written https://brigit5.azurewebsites.net/ResearchStudy/2105b30c-7d6b-4a59-927e-70b7803d0c93/_history/1
// https://brigit5.azurewebsites.net/ResearchStudy/b3c9d8d8-b6cd-4368-8d60-ed6b21362385/_history/1
//

using Hl7.Fhir.Model;  // https://docs.fire.ly/firelynetsdk/index.html
using Hl7.Fhir.Rest;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Net;
using System.Text;
using BW_Events;
using System.Security.Policy;
using System.Collections.ObjectModel;

namespace SoA.FHIR
{
    public class SoA_FHIR : INotifyPropertyChanged // use dependency properties
    {

        #region Events
        /// <summary>
        /// Declares a handler for changes in properties within the class.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Methods

        /// <summary>
        /// Fires the property changed event 
        /// </summary>
        /// <remarks>
        /// This method is called by the Set accessor of each property.
        /// The CallerMemberName attribute that is applied to the optional propertyName
        /// parameter causes the property name of the caller to be substituted as an argument.
        /// </remarks>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Methods

        #region properties

        static public string Server = "https://brigit5.azurewebsites.net";

        static public string RetVal = "not run";

        static public FhirClient FHIRClient = null;

        private ResearchStudy researchStudy = default(ResearchStudy);
        public ResearchStudy ResearchStudy
        {
            get
            { return researchStudy; }
            set
            {
                if (value != researchStudy)
                {
                    researchStudy = value;
                    this.NotifyPropertyChanged(nameof(ResearchStudy));
                }
            }
        }


        private BindingList<ActivityDefinition> activitiesList = default(BindingList<ActivityDefinition>);
        public BindingList<ActivityDefinition> ActivitiesList
        {
            get
            { return activitiesList; }
            set
            {
                if (value != activitiesList)
                {
                    activitiesList = value;
                    this.NotifyPropertyChanged(nameof(ActivitiesList));
                }
            }
        }

        private PlanDefinition baseVisit = default(PlanDefinition);
        public PlanDefinition BaseVisit
        {
            get
            { return baseVisit; }
            set
            {
                if (value != baseVisit)
                {
                    baseVisit = value;
                    this.NotifyPropertyChanged(nameof(BaseVisit));
                }
            }
        }


        private BindingList<PlanDefinition> visitsList = default(BindingList<PlanDefinition>);
        public BindingList<PlanDefinition> VisitsList
        {
            get
            { return visitsList; }
            set
            {
                if (value != visitsList)
                {
                    visitsList = value;
                    this.NotifyPropertyChanged(nameof(VisitsList));
                }
            }
        }


        #endregion properties

        public void Clear()
        {
            this.ResearchStudy = default(ResearchStudy);
            this.ActivitiesList = new BindingList<ActivityDefinition>();
            this.BaseVisit = default(PlanDefinition);
            this.VisitsList = new BindingList<PlanDefinition>();
        }


        static bool createFHIRClient()
        {
            if (FHIRClient == null)
            {
                FHIRClient = new FhirClient(Server);
                FHIRClient.PreferredFormat = ResourceFormat.Json;
                FHIRClient.Timeout = 120000; // The timeout is set in milliseconds, with a default of 100000

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }
            return true;
        }

        public string WriteActivityString(string argID,
            string argStartDate,
            string argEndDate,
            string argName,
            string argDependendsOn)
        {
            ActivityDefinition activity = WriteActivity(argID, argStartDate, argEndDate, argName, argDependendsOn);
            return activity.Id;
        }

        public ActivityDefinition WriteActivity(string argID,
            string argStartDate,
            string argEndDate,
            string argName,
            string argDependendsOn)
        {
            createFHIRClient();

            ActivityDefinition activity = new ActivityDefinition();
            activity.Status = PublicationStatus.Active;

            activity.Identifier.Add(new Identifier("bluewaveinformatics.co.uk", argID));
            activity.Title = argName;
            {
                FhirDateTime periodStart = new FhirDateTime(argStartDate);
                FhirDateTime periodEnd = new FhirDateTime(argEndDate);
                Period period = new Period(periodStart, periodEnd);
                activity.Timing = period;
            }

            ActivityDefinition created_activity = FHIRClient.Create<ActivityDefinition>(activity);
            return created_activity;

        }

        public bool WriteAllActivities(EventSheet argEventSheet)
        {
            ActivityDefinition activity = null;

            this.ActivitiesList = new BindingList<ActivityDefinition>();

            foreach( EventRow eventRow in argEventSheet.Rows )
            {
                if (eventRow.Type == "A")
                {
                    activity = WriteActivity(eventRow.ID, "", "", eventRow.Name, eventRow.DependsOn);
                    this.ActivitiesList.Add(activity);
                    foreach ( EventGridRow gridRow in argEventSheet.GridRows)
                    {
                        if( gridRow.ActivityID == eventRow.ID)
                        {
                            gridRow.ActivityResourceID = activity.Id;
                            gridRow.ActivityName = activity.Title;
                        }
                    }
                }
            }

            return true;
        }

        public bool WriteVisitBase(EventSheet argEventSheet)
        {
            createFHIRClient();

            this.BaseVisit = new PlanDefinition();
            this.BaseVisit.Title = "Base visit";
            this.BaseVisit.Status = PublicationStatus.Active;
            this.BaseVisit.Action = new List<PlanDefinition.ActionComponent>();
            PlanDefinition.ActionComponent action = new PlanDefinition.ActionComponent();
            action.Title = "Base";
            action.ElementId = Guid.NewGuid().ToString(); 
            this.BaseVisit.Action.Add(action);
            this.BaseVisit = FHIRClient.Create<PlanDefinition>(this.BaseVisit);

            return true;
        }

        public PlanDefinition WriteVisit(string argID,
            string argStartDate,
            string argEndDate,
            int argDayNum,
            int argWeekNum,
            string argName,
            string argDependendsOn,
            List<PlanDefinition.ActionComponent> argComponents)
        {
            createFHIRClient();

            PlanDefinition visit = new PlanDefinition();
            visit.Status = PublicationStatus.Active;

            visit.Identifier.Add(new Identifier("bluewaveinformatics.co.uk", argID));
            visit.Title = argName; 

            visit.Action = argComponents;

            PlanDefinition.RelatedActionComponent relatedActionComponent = new PlanDefinition.RelatedActionComponent();
            relatedActionComponent.ActionId = this.BaseVisit.Action[0].ElementId;
            relatedActionComponent.Relationship = ActionRelationshipType.AfterStart;
            
            Duration duration = new Duration();
            duration.Value = argDayNum;
            duration.System = "http://hl7.org/fhir/ValueSet/all-time-units";
            duration.Code = "d";
            relatedActionComponent.Offset = duration;

            foreach (PlanDefinition.ActionComponent actionComponent in visit.Action )
            {
                actionComponent.RelatedAction.Add(relatedActionComponent);
            }

            visit = FHIRClient.Create<PlanDefinition>(visit);
            return visit;
        }



        public bool WriteAllVisits(EventSheet argEventSheet)
        {
            string identifier = default(string);
            this.VisitsList = new BindingList<PlanDefinition>();

            if( ActivitiesList.Count <=0)
            {
                return false;
            }

            PlanDefinition visit = null;

            foreach (EventRow eventRow in argEventSheet.Rows)
            {
                if (eventRow.Type == "V")
                {
                    List<PlanDefinition.ActionComponent> components = new List<PlanDefinition.ActionComponent>();

                    foreach (EventGridRow gridRow in argEventSheet.GridRows)
                    {
                        if (gridRow.VisitID == eventRow.ID)
                        {
                            
                             /// Now find the activity and add its reference to the components list                           
                            foreach(ActivityDefinition activityDefinition in ActivitiesList)
                            {
                                if( activityDefinition.Id == gridRow.ActivityResourceID)
                                {
                                    PlanDefinition.ActionComponent actionComponent = new PlanDefinition.ActionComponent();
                                    actionComponent.Title = activityDefinition.Title;
                                    actionComponent.Definition = new Canonical(Server+"/ActivityDefinition/"+activityDefinition.IdElement);
                                    components.Add(actionComponent);
                                }
                            }                           
                        }
                    }

                    visit = WriteVisit(eventRow.ID, eventRow.StartDate, eventRow.EndDate, eventRow.DayNum, eventRow.WeekNum, eventRow.Name, eventRow.DependsOn, components);
                    VisitsList.Add(visit);

                    // Next step is required to have the list of identifers to add to the high level structure
                    foreach (EventGridRow gridRow in argEventSheet.GridRows)
                    {
                        if (gridRow.VisitID == eventRow.ID)
                        {
                            gridRow.VisitResourceID = visit.Id;
                        }
                    }
                }
            }

            return true;
        }
    
        public string WriteResearchStudy(Study argStudy, EventSheet argEventSheet)
        {
            createFHIRClient();

            this.ResearchStudy = new ResearchStudy();
            this.ResearchStudy.Identifier = new List<Identifier>();

            this.ResearchStudy.Identifier.Add(new Identifier("bluewaveinformatics.co.uk", argStudy.StudyID));
            this.ResearchStudy.Title = argStudy.StudyName;

            this.ResearchStudy.Status = ResearchStudy.ResearchStudyStatus.Active;

            this.ResearchStudy.Protocol = new List<ResourceReference>();

            { 
                ResourceReference resourceReference = new ResourceReference();
                resourceReference.ElementId = this.BaseVisit.Id;
                resourceReference.Type = "PlanDefinition";
                resourceReference.Display = "BaseVisit";
                this.ResearchStudy.Protocol.Add(resourceReference);
            }

            foreach( PlanDefinition visit in VisitsList)
            {
                ResourceReference resourceReference = new ResourceReference();
                resourceReference.ElementId = visit.Id;
                resourceReference.Type = "PlanDefinition";
                resourceReference.Display = visit.Title;
                this.ResearchStudy.Protocol.Add(resourceReference);
            }

            this.ResearchStudy = FHIRClient.Create<ResearchStudy>(this.ResearchStudy);

            return this.ResearchStudy.ResourceIdentity(Server).ToString();
        }

        public string LoadResearchStudy(string argLocation)
        {
            createFHIRClient();
            this.Clear();

            this.ResearchStudy = FHIRClient.Read<ResearchStudy>(argLocation);
            return this.ResearchStudy.Title;
        }

        public bool LoadVisits()
        {
            createFHIRClient();

            foreach ( ResourceReference vistRef in this.ResearchStudy.Protocol)
            {
                string tmp = Server + "/PlanDefinition/" + vistRef.ElementId;
                PlanDefinition visit = LoadVisit(tmp);
                if( visit.Title == "Base visit")
                {
                    this.BaseVisit = visit;
                }
                else
                {
                    this.VisitsList.Add(visit);
                }
            }

            return true;
        }

        public PlanDefinition LoadVisit(string argElementId)
        {
            PlanDefinition visit = FHIRClient.Read<PlanDefinition>(argElementId);
            return visit;
        }



        }
}
