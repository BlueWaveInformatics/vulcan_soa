﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BW_Events
{
    public class Study : INotifyPropertyChanged
    {

        #region Events
        /// <summary>
        /// Declares a handler for changes in properties within the class.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Methods

        /// <summary>
        /// Fires the property changed event 
        /// </summary>
        /// <remarks>
        /// This method is called by the Set accessor of each property.
        /// The CallerMemberName attribute that is applied to the optional propertyName
        /// parameter causes the property name of the caller to be substituted as an argument.
        /// </remarks>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Methods

        #region Properties

        private string fileName = default(string);
        public string FileName
        {
            get
            { return fileName; }
            set
            {
                if (value != fileName)
                {
                    fileName = value;
                    this.NotifyPropertyChanged(nameof(FileName));
                }
            }
        }

        private string studyName = default(string);
        public string StudyName
        {
            get
            { return studyName; }
            set
            {
                if (value != studyName)
                {
                    studyName = value;
                    this.NotifyPropertyChanged(nameof(StudyName));
                }
            }
        }

        private string studyID = default(string);
        public string StudyID
        {
            get
            { return studyID; }
            set
            {
                if (value != studyID)
                {
                    studyID = value;
                    this.NotifyPropertyChanged(nameof(StudyID));
                }
            }
        }

        #endregion Properties

        protected Study()
        { }

        public static Study New( Study argStudy)
        {
            if (argStudy is null)
            {
                argStudy = new Study();
            }

            argStudy.FileName = "<empty>";
            argStudy.StudyID = "<empty>";
            argStudy.StudyName = "<empty>";
            return argStudy;
        }

    }
}
