﻿using BW_Events.Properties;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;

namespace BW_Events
{
    public class Spreadsheet_File : INotifyPropertyChanged
    {

        #region Events
        /// <summary>
        /// Declares a handler for changes in properties within the class.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Methods

        /// <summary>
        /// Fires the property changed event 
        /// </summary>
        /// <remarks>
        /// This method is called by the Set accessor of each property.
        /// The CallerMemberName attribute that is applied to the optional propertyName
        /// parameter causes the property name of the caller to be substituted as an argument.
        /// </remarks>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Methods

        #region Properties

        private string fileName = default(string);
        public string FileName
        {
            get
            { return fileName; }
            set
            {
                if (value != fileName)
                {
                    fileName = value;
                    this.NotifyPropertyChanged(nameof(FileName));
                }
            }
        }

        private EventSheet eventSheet = default(EventSheet);
        public EventSheet EventSheet
        {
            get
            { return eventSheet; }
            set
            {
                if (value != eventSheet)
                {
                    eventSheet = value;
                    this.NotifyPropertyChanged(nameof(EventSheet));
                }
            }
        }

        private List<string> activityDefNameList = default(List<string>);
        public List<string> ActivityDefNameList
        {
            get
            { return activityDefNameList; }
            set
            {
                if (value != activityDefNameList)
                {
                    activityDefNameList = value;
                    this.NotifyPropertyChanged(nameof(ActivityDefNameList));
                }
            }
        }

        private List<string> eventDefNameList = default(List<string>);
        public List<string> EventDefNameList
        {
            get
            { return eventDefNameList; }
            set
            {
                if (value != eventDefNameList)
                {
                    eventDefNameList = value;
                    this.NotifyPropertyChanged(nameof(EventDefNameList));
                }
            }
        }

        private List<string> formDefNameList = default(List<string>);
        public List<string> FormDefNameList
        {
            get
            { return formDefNameList; }
            set
            {
                if (value != formDefNameList)
                {
                    formDefNameList = value;
                    this.NotifyPropertyChanged(nameof(FormDefNameList));
                }
            }
        }
        #endregion Properties


        protected Spreadsheet_File()
        { }

        public static Spreadsheet_File New()
        {
            Spreadsheet_File file = new Spreadsheet_File();
            file.FileName = "<empty>";
            file.ActivityDefNameList = new List<string>();
            file.EventDefNameList = new List<string>();
            file.FormDefNameList = new List<string>();
            file.EventSheet = EventSheet.New();
            return file;
        }
        public bool LoadSpreadsheet(Study argStudy, FileInfo argFileInfo)
        {
            argFileInfo = new System.IO.FileInfo(@"C:\Filing\FHIR Apps\SoA_01\Data\ScheduleOfActivities_01.xlsx");
            //argFileInfo = new System.IO.FileInfo(@"C:\Filing\Programming Tests\FHIR\ResearchSubject_01\Data\ScheduleOfActivities_01.xls");
            //argFileInfo = new System.IO.FileInfo(@"C:\Users\hugh\Blue Wave Informatics LLP\File Store - H Drive\Programming Tests\FHIR\ResearchSubject_01\Data\ScheduleOfActivities_01.xls");


            if (!argFileInfo.Exists)
            {
                MessageBox.Show("File not found", "Import", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            FileName = argFileInfo.Name;
            FileStream stream = argFileInfo.Open(FileMode.Open,FileAccess.Read,FileShare.Read);

            //EventSheet = EventSheet.New();

            try
            {
                using (ExcelPackage excelPackage = new ExcelPackage(argFileInfo))
                {
                    excelPackage.Load(stream);
                     var count = excelPackage?.Workbook?.Worksheets?.Count;
                    if( count <=0 )
                    {
                        MessageBox.Show("Excel file has no valid worksheets");
                        return false;
                    }
                    foreach (ExcelWorksheet ws in excelPackage?.Workbook.Worksheets)
                    {
                        if (ws.Name == "READ ME")
                        {
                        }
                        else if (ws.Name=="Simple")
                        {
                            if (String.Equals((string)ws.Cells["A1"].Text, "Study ID"))
                            {
                                string studyID = (string)ws.Cells["B1"].Text;
                            }

                            int blanks = 0;
                            for (int rowNum = 2; rowNum <= ws.Cells.Rows && blanks < Settings.Default.MaxBlankRows; rowNum++)
                            {
                                string id = ws.Cells[rowNum, 1].Text;
                                if (string.IsNullOrWhiteSpace(id) != true)
                                {
                                    this.EventSheet.Add(
                                        "A",
                                        id,
                                        (string)ws.Cells[rowNum, 2].Text,
                                        (string)ws.Cells[rowNum, 3].Text,
                                        (string)ws.Cells[rowNum, 4].Text,
                                        (string)ws.Cells[rowNum, 1].Text);
                                }
                            }
                        }
                        else if (ws.Name == "H2Q-MC-LZZT(c)")
                        {
                            string id, dependsOn;
                            int blankRows = 0;
                            int blankColumns = 0;
                            int studyIDRow = 0;
                            int visitIDRow = 0;
                            int activityRowNum = 0;
                            List<int> activityRowNums = new List<int>();
                            string activityName = null;
                            List<IDNumPair> visitColNums = new List<IDNumPair>();

                            // Find Study ID
                            for (int rowNum = 1; rowNum <= ws.Cells.Rows && blankRows < Settings.Default.MaxBlankRows; rowNum++)
                            {
                                if (String.Equals((string)ws.Cells[rowNum, 1].Text.ToUpper(), "STUDY ID"))
                                {
                                    argStudy.StudyID = (string)ws.Cells[rowNum, 2].Text;
                                    argStudy.StudyName = (string)ws.Cells[rowNum+1, 2].Text;
                                    studyIDRow = rowNum;
                                }
                            }

                            // Find the start of the Activities
                            for (int rowNum = studyIDRow + 1; rowNum <= ws.Cells.Rows && blankRows < Settings.Default.MaxBlankRows; rowNum++)
                            {
                                activityName = ws.Cells[rowNum, 1].Text;
                                if (string.IsNullOrWhiteSpace(activityName) != true)
                                {
                                    if (String.Equals(activityName.ToUpper(), "ACTIVITY"))
                                    {
                                        activityRowNum = rowNum + 1;
                                        break;
                                    }

                                }
                            }

                            // Read Activities
                            for (int rowNum =activityRowNum; rowNum <= ws.Cells.Rows && blankRows < Settings.Default.MaxBlankRows; rowNum++)
                            {
                                activityName = ws.Cells[rowNum, 1].Text;
                                if (string.IsNullOrWhiteSpace(activityName) != true)
                                {
                                    if (String.Equals(activityName.ToUpper(), "NOTES"))
                                    {
                                        break;
                                    }

                                    ActivityDefNameList.Add(rowNum.ToString() + " " + activityName);
                                    activityRowNums.Add(rowNum);
                                    this.EventSheet.Add(
                                        "A",
                                        rowNum.ToString(),
                                        "",
                                        "",
                                        activityName,
                                        "");
                                }
                            }

                            // Read Visits
                            for (int rowNum = studyIDRow + 1; rowNum <= ws.Cells.Rows && blankRows < Settings.Default.MaxBlankRows; rowNum++)
                            {
                                if (String.Equals((string)ws.Cells[rowNum, 2].Text.ToUpper(), "VISIT"))
                                {
                                    string prevVisitID = "";
                                    for (int colNum = 3; colNum <= ws.Cells.Columns && blankColumns < Settings.Default.MaxBlankColumns; colNum++)
                                    { // Read the visits
                                        string visitID = ws.Cells[rowNum, colNum].Text;
                                        if (string.IsNullOrWhiteSpace(visitID) != true)
                                        {
                                            visitColNums.Add(new IDNumPair(visitID, colNum));
                                            int wkNum = 99;
                                                if( Int32.TryParse((string)ws.Cells[rowNum + 2, colNum].Text, out wkNum)==false )
                                                    { wkNum = 0; }
                                            int dayNum = 99;
                                            if (Int32.TryParse((string)ws.Cells[rowNum + 3, colNum].Text, out dayNum) == false)
                                            { dayNum = 0; }

                                            EventDefNameList.Add(visitID + " " + (string)ws.Cells[rowNum + 1, colNum].Text);

                                            this.EventSheet.Add(
                                                "V",
                                                visitID,
                                                wkNum,
                                                dayNum,
                                                (string)ws.Cells[rowNum + 1, colNum].Text,
                                                prevVisitID);
                                            prevVisitID = visitID;
                                        }

                                    }
                                    break;
                                }
                            }

                            { // Read the spreadsheet creating a list of column/row pairs where there is ann X (or similar) in the cell
                                int rowNum, colNum;
                                for( int i = 0; i< visitColNums.Count; i++)
                                {
                                    colNum = visitColNums[i].num;
                                    for( int j =0; j<activityRowNums.Count; j++)
                                    {
                                        rowNum = activityRowNums[j];
                                        if (string.IsNullOrWhiteSpace((string)ws.Cells[rowNum, colNum].Text) != true)
                                        {
                                            this.EventSheet.AddEventGridRow(visitColNums[i].ID,rowNum.ToString(),"","","","");
                                        }
                                    }
                                }
                            }

                            /// Then write an activity and write the activity ID into every cell in this table
                            /// Then write the plan definnition and read and add the ids of all activities from the table
                            /// 
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Helpers.ExceptionReport.ShowDialog("Could not open file for importing", ex);
                return false;
            }


            MessageBox.Show("Excel file read");
            return true;
          
        }



    }
}
