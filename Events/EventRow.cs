﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BW_Events
{
    public class EventRow : INotifyPropertyChanged // use dependency properties
    {
        #region Events
        /// <summary>
        /// Declares a handler for changes in properties within the class.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Methods

        /// <summary>
        /// Fires the property changed event 
        /// </summary>
        /// <remarks>
        /// This method is called by the Set accessor of each property.
        /// The CallerMemberName attribute that is applied to the optional propertyName
        /// parameter causes the property name of the caller to be substituted as an argument.
        /// </remarks>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Methods

        #region Properties

        private string type = default(string);
        public string Type
        {
            get
            { return type; }
            set
            {
                if (value != type)
                {
                    type = value;
                    this.NotifyPropertyChanged(nameof(Type));
                }
            }
        }

        private string id = default(string);
        public string ID
        {
            get
            { return id; }
            set
            {
                if (value != id)
                {
                    id = value;
                    this.NotifyPropertyChanged(nameof(ID));
                }
            }
        }

        private string startDate = default(string);
        public string StartDate
        {
            get
            { return startDate; }
            set
            {
                if (value != startDate)
                {
                    startDate = value;
                    this.NotifyPropertyChanged(nameof(StartDate));
                }
            }
        }

        private string endDate = default(string);
        public string EndDate
        {
            get
            { return endDate; }
            set
            {
                if (value != endDate)
                {
                    endDate = value;
                    this.NotifyPropertyChanged(nameof(EndDate));
                }
            }
        }

        private int weekNum = default(int);
        public int WeekNum
        {
            get
            { return weekNum; }
            set
            {
                if (value != weekNum)
                {
                    weekNum = value;
                    this.NotifyPropertyChanged(nameof(WeekNum));
                }
            }
        }

        private int dayNum = default(int);
        public int DayNum
        {
            get
            { return dayNum; }
            set
            {
                if (value != dayNum)
                {
                    dayNum = value;
                    this.NotifyPropertyChanged(nameof(DayNum));
                }
            }
        }

        private string name = default(string);
        public string Name
        {
            get
            { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    this.NotifyPropertyChanged(nameof(Name));
                }
            }
        }

        private string dependsOn = default(string);
        public string DependsOn
        {
            get
            { return dependsOn; }
            set
            {
                if (value != dependsOn)
                {
                    dependsOn = value;
                    this.NotifyPropertyChanged(nameof(DependsOn));
                }
            }
        }

        #endregion Properties

        protected EventRow()
        { }

        public static EventRow New(
            string argType,
            string argID,
            string argStartDate,
            string argEndDate,
            string argName,
            string argDependsOn)
        {
            EventRow importRow = new EventRow();
            importRow.Type = argType;
            importRow.ID = argID;
            importRow.StartDate = argStartDate.Trim();
            importRow.EndDate = argEndDate.Trim();
            importRow.Name = argName.Trim();
            importRow.DependsOn = argDependsOn;

            return importRow;
        }

        public static EventRow New(
            string argType, 
            string argID,
            int argWeekNum,
            int argDayNum,
            string argName,
            string argDependsOn)
        {
            EventRow importRow = new EventRow();
            importRow.Type = argType;
            importRow.ID = argID;
            importRow.WeekNum = argWeekNum;
            importRow.DayNum = argDayNum;
            importRow.Name = argName.Trim();
            importRow.DependsOn = argDependsOn;

            return importRow;
        }


    }

    public class EventGridRow : INotifyPropertyChanged // use dependency properties
    {
        #region Events
        /// <summary>
        /// Declares a handler for changes in properties within the class.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Methods

        /// <summary>
        /// Fires the property changed event 
        /// </summary>
        /// <remarks>
        /// This method is called by the Set accessor of each property.
        /// The CallerMemberName attribute that is applied to the optional propertyName
        /// parameter causes the property name of the caller to be substituted as an argument.
        /// </remarks>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Methods

        #region Properties
        private string visitID = default(string);
        public string VisitID
        {
            get
            { return visitID; }
            set
            {
                if (value != visitID)
                {
                    visitID = value;
                    this.NotifyPropertyChanged(nameof(VisitID));
                }
            }
        }

        private string activityID = default(string);
        public string ActivityID
        {
            get
            { return activityID; }
            set
            {
                if (value != activityID)
                {
                    activityID = value;
                    this.NotifyPropertyChanged(nameof(ActivityID));
                }
            }
        }

        private string visitResourceID = default(string);
        public string VisitResourceID
        {
            get
            { return visitResourceID; }
            set
            {
                if (value != visitResourceID)
                {
                    visitResourceID = value;
                    this.NotifyPropertyChanged(nameof(VisitResourceID));
                }
            }
        }

        private string activityResourceID = default(string);
        public string ActivityResourceID
        {
            get
            { return activityResourceID; }
            set
            {
                if (value != activityResourceID)
                {
                    activityResourceID = value;
                    this.NotifyPropertyChanged(nameof(ActivityResourceID));
                }
            }
        }

        private string visitName = default(string);
        public string VisitName
        {
            get
            { return visitName; }
            set
            {
                if (value != visitName)
                {
                    visitName = value;
                    this.NotifyPropertyChanged(nameof(VisitName));
                }
            }
        }

        private string activityName = default(string);
        public string ActivityName
        {
            get
            { return activityName; }
            set
            {
                if (value != activityName)
                {
                    activityName = value;
                    this.NotifyPropertyChanged(nameof(ActivityName));
                }
            }
        }


        #endregion Properties

        protected EventGridRow() { }

        public static EventGridRow New(
            string argVisitID,
            string argActivityID,
            string argVisitResourceID,
            string argActivityResourceID,
            string argVisitName,
            string argActivityName)
        {
            EventGridRow eventGridRow = new EventGridRow();
            eventGridRow.visitID = argVisitID;
            eventGridRow.activityID = argActivityID;
            eventGridRow.visitResourceID = argVisitResourceID;
            eventGridRow.activityResourceID = argActivityResourceID;
            eventGridRow.visitName = argVisitName;
            eventGridRow.activityName = argActivityName;
            return eventGridRow;
        }
    }

    public class IDNumPair
    {
        public string ID;
        public int num;

        public IDNumPair(string argID, int argNum)
        {
            ID = argID;
            num = argNum;
        }
    }

}
