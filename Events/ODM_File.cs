﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace BW_Events
{
    public class ODM_File : INotifyPropertyChanged
    {
        #region Events
        /// <summary>
        /// Declares a handler for changes in properties within the class.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Methods

        /// <summary>
        /// Fires the property changed event 
        /// </summary>
        /// <remarks>
        /// This method is called by the Set accessor of each property.
        /// The CallerMemberName attribute that is applied to the optional propertyName
        /// parameter causes the property name of the caller to be substituted as an argument.
        /// </remarks>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Methods

        #region Properties

        private string fileName = default(string);
        public string FileName
        {
            get
            { return fileName; }
            set
            {
                if (value != fileName)
                {
                    fileName = value;
                    this.NotifyPropertyChanged(nameof(FileName));
                }
            }
        }

        private EventSheet eventSheet = default(EventSheet);
        public EventSheet EventSheet
        {
            get
            { return eventSheet; }
            set
            {
                if (value != eventSheet)
                {
                    eventSheet = value;
                    this.NotifyPropertyChanged(nameof(EventSheet));
                }
            }
        }

        private List<XElement> activityDefList = default(List<XElement>);
        public List<XElement> ActivityDefList
        {
            get
            { return activityDefList; }
            set
            {
                if (value != activityDefList)
                {
                    activityDefList = value;
                    this.NotifyPropertyChanged(nameof(ActivityDefList));
                }
            }
        }

        private List<string> activityDefNameList = default(List<string>);
        public List<string> ActivityDefNameList
        {
            get
            { return activityDefNameList; }
            set
            {
                if (value != activityDefNameList)
                {
                    activityDefNameList = value;
                    this.NotifyPropertyChanged(nameof(ActivityDefNameList));
                }
            }
        }


        private List<XElement> eventDefList = default(List<XElement>);
        public List<XElement> EventDefList
            {
            get
            { return eventDefList; }
            set
            {
                if (value != eventDefList)
                {
                    eventDefList = value;
                    this.NotifyPropertyChanged(nameof(EventDefList));
                }
            }
        }

        private List<string> eventDefNameList = default(List<string>);
        public List<string> EventDefNameList
        {
            get
            { return eventDefNameList; }
            set
            {
                if (value != eventDefNameList)
                {
                    eventDefNameList = value;
                    this.NotifyPropertyChanged(nameof(EventDefNameList));
                }
            }
        }

        private List<XElement> formDefList = default(List<XElement>);
        public List<XElement> FormDefList
        {
            get
            { return formDefList; }
            set
            {
                if (value != formDefList)
                {
                    formDefList = value;
                    this.NotifyPropertyChanged(nameof(FormDefList));
                }
            }
        }

        private List<string> formDefNameList = default(List<string>);
        public List<string> FormDefNameList
        {
            get
            { return formDefNameList; }
            set
            {
                if (value != formDefNameList)
                {
                    formDefNameList = value;
                    this.NotifyPropertyChanged(nameof(FormDefNameList));
                }
            }
        }

        private List<XElement> itemDefList    = default(List<XElement>);
        public List<XElement> ItemDefList
        {
            get
            { return itemDefList; }
            set
            {
                if (value != itemDefList)
                {
                    itemDefList = value;
                    this.NotifyPropertyChanged(nameof(ItemDefList));
                }
            }
        }

        private List<string> itemDefNameList = default(List<string>);
        public List<string> ItemDefNameList
        {
            get
            { return itemDefNameList; }
            set
            {
                if (value != itemDefNameList)
                {
                    itemDefNameList = value;
                    this.NotifyPropertyChanged(nameof(ItemDefNameList));
                }
            }
        }



        #endregion Properties

        protected ODM_File()
        { }

        public static ODM_File New()
        {
            ODM_File file = new ODM_File();
            file.FileName = "<empty>";
            file.ActivityDefNameList = new List<string>();
            file.EventDefNameList = new List<string>();
            file.FormDefNameList = new List<string>();
            file.itemDefNameList = new List<string>();
            file.EventSheet = EventSheet.New();
            return file;
        }

        public bool LoadODMFile(Study argStudy, FileInfo argFileInfo)
        {
            argFileInfo = new System.IO.FileInfo(@"C:\Filing\FHIR Apps\SoA_01\Data\LZZTTrial.xml");


            if (!argFileInfo.Exists)
            {
                MessageBox.Show("File not found", "Import", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            FileName = argFileInfo.Name;
            FileStream stream = argFileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);

            //EventSheet = EventSheet.New();

            try
            {
                XNamespace ns = @"http://www.cdisc.org/ns/odm/v1.3";
                XNamespace sdm = @"http://www.cdisc.org/ns/studydesign/v1.0"; 
                
                // Loading from a file
                var xml = XDocument.Load(stream);

                XElement root = xml.Root;

                // Load Study ID and Name
                List<XElement> IDList = xml.Descendants().Where(p => p.Name.LocalName == "Study").ToList();
                argStudy.StudyID = IDList[0].Attribute("OID").Value;
                
                List<XElement> NameList = xml.Descendants().Where(p => p.Name.LocalName == "StudyName").ToList();
                argStudy.StudyName = NameList[0].Value;


                EventDefList = xml.Descendants().Where(p => p.Name.LocalName == "StudyEventDef").ToList();
                ActivityDefList = xml.Descendants().Where(p => p.Name.LocalName == "ActivityDef").ToList();
                FormDefList = xml.Descendants().Where(p => p.Name.LocalName == "FormDef").ToList();
                ItemDefList = xml.Descendants().Where(p => p.Name.LocalName == "ItemDef").ToList();

                //List<XElement> result = xml.Root.Element(ns+"Study")?.Elements(ns+"MetaDataVersion")?.Elements(ns+"StudyEventDef").ToList();

                // Visits

                string prevVisitID = "";
                List<string> visitOIDs = new List<string>();

                int wkNum = 0;
                int dayNum = 0;

                foreach (XElement element in EventDefList)
                {
                    EventDefNameList.Add(element.Attribute("OID").Value + "  " + element.Attribute("Name").Value);


                    List<XElement> FormRefList = element.Descendants().Where(p => p.Name.LocalName == "FormRef").ToList();
                    visitOIDs.Add(element.Attribute("OID").Value);
                    foreach( XElement formElement in FormRefList)
                    {
                        List<XElement> formNames = xml.Descendants().Where(p => p.Name.LocalName == "FormDef" && p.Attribute("OID").Value== formElement.Attribute("FormOID").Value).ToList();
                        this.EventSheet.AddEventGridRow(element.Attribute("OID").Value, formElement.Attribute("FormOID").Value, "", "", element.Attribute("Name").Value, formNames[0].Attribute("Name").Value);
                    }

                    this.EventSheet.Add(
                                        "V",
                                        element.Attribute("OID").Value,
                                        wkNum,
                                        dayNum,
                                        element.Attribute("Name").Value,
                                        prevVisitID);
                    wkNum++;
                    dayNum += 7;
                    prevVisitID = element.Attribute("OID").Value;
                }

                // Activities
                foreach (XElement element in ActivityDefList)
                {
                    ActivityDefNameList.Add(element.Attribute("OID").Value + "  " + element.Attribute("Name").Value);
                }

                foreach (XElement element in FormDefList)
                {
                    FormDefNameList.Add(element.Attribute("OID").Value + "  " + element.Attribute("Name").Value);

                    this.EventSheet.Add(
                    "A",
                    element.Attribute("OID").Value,
                    "",
                    "",
                    element.Attribute("Name").Value,
                    "");
                }

                foreach (XElement element in ItemDefList)
                {
                    ItemDefNameList.Add(element.Attribute("OID").Value + "  " + element.Attribute("Name").Value);
                }


            }

            catch (Exception ex)
            {
                Helpers.ExceptionReport.ShowDialog("Could not open file for importing", ex);
                return false;
            }


            MessageBox.Show("ODM file read");
            return true;

        }
    }
}
