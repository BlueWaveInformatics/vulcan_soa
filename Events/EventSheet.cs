﻿using BW_Events.Properties;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;

namespace BW_Events
{
    public class EventSheet : INotifyPropertyChanged
    {
        #region Events
        /// <summary>
        /// Declares a handler for changes in properties within the class.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Methods

        /// <summary>
        /// Fires the property changed event 
        /// </summary>
        /// <remarks>
        /// This method is called by the Set accessor of each property.
        /// The CallerMemberName attribute that is applied to the optional propertyName
        /// parameter causes the property name of the caller to be substituted as an argument.
        /// </remarks>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Methods

        #region Properties

        private BindingList<EventRow> rows = default(BindingList<EventRow>);
        public BindingList<EventRow> Rows
        {
            get
            { return rows; }
            set
            {
                if (value != rows)
                {
                    rows = value;
                    this.NotifyPropertyChanged(nameof(Rows));
                }
            }
        }

        private BindingList<EventGridRow> gridRows = default(BindingList<EventGridRow>);
        public BindingList<EventGridRow> GridRows
        {
            get
            { return gridRows; }
            set
            {
                if (value != gridRows)
                {
                    gridRows = value;
                    this.NotifyPropertyChanged(nameof(GridRows));
                }
            }
        }



        #endregion Properties

        protected EventSheet()
        {}

        public static EventSheet New()
        { 
            EventSheet sheet = new EventSheet();
            sheet.Rows = new BindingList<EventRow>();
            sheet.GridRows = new BindingList<EventGridRow>();
            //sheet.FileName = "<empty>";
            return sheet;
        }

        public void Clear()
        {
            this.Rows.Clear();
        }

        public EventRow Add(
            string argType,
            string argID, 
            string argStartDate, 
            string argEndDate, 
            string argName, 
            string argDependendsOn)
        {
            EventRow row = EventRow.New(argType,
            argID,
            argStartDate,
            argEndDate,
            argName,
            argDependendsOn);

            ///TODO check what Add does with a null row
            Rows.Add(row);

            return row;
        }

        public EventRow Add(
            string argType,
            string argID,
            int argWeekNum,
            int argDayNum,
            string argName,
            string argDependendsOn)
        {
            EventRow row = EventRow.New(argType,
            argID,
            argWeekNum,
            argDayNum,
            argName,
            argDependendsOn);

            ///TODO check what Add does with a null row
            Rows.Add(row);

            return row;
        }

        public EventGridRow AddEventGridRow(
            string argVisitID,
            string argActivityID,
            string argVisitResourceID,
            string argActivityResourceID,
            string argVisitName,
            string argActivityName
            )
        {
            EventGridRow eventGridRow = EventGridRow.New(
            argVisitID,
            argActivityID,
            argVisitResourceID,
            argActivityResourceID,
            argVisitName,
            argActivityName);
            GridRows.Add(eventGridRow);

            return eventGridRow;
        }

    }
}
