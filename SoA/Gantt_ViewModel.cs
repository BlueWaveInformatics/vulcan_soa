﻿using Hl7.Fhir.Model;  // https://docs.fire.ly/firelynetsdk/index.html
using Hl7.Fhir.Rest;

using SoA.FHIR;
using Syncfusion.Windows.Controls.Gantt;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SoA
{
    public class Gantt_ViewModel
    {
        public ObservableCollection<TaskDetails> TaskCollection { get; set; }
        public Gantt_ViewModel()
        {
            TaskCollection = this.GetDataSource();
        }

        private ObservableCollection<TaskDetails> GetDataSource()
        {
            ObservableCollection<TaskDetails> task = new ObservableCollection<TaskDetails>();
            task.Add(
                new TaskDetails
                {
                    TaskId = 1,
                    TaskName = "Protocol",
                    StartDate = new DateTime(2021, 1, 1),
                    FinishDate = new DateTime(2021, 1, 3),
                    Progress = 3d
                });

            //task[0].Child.Add(
            //    new TaskDetails
            //    {
            //        TaskId = 2,
            //        TaskName = "Determine project office scope",
            //        StartDate = new DateTime(2021, 1, 3),
            //        FinishDate = new DateTime(2021, 1, 5),
            //        Progress = 20d
            //    });

            //task[0].Child.Add(
            //    new TaskDetails
            //    {
            //        TaskId = 3,
            //        TaskName = "Justify project office via business model",
            //        StartDate = new DateTime(2021, 1, 6),
            //        FinishDate = new DateTime(2021, 1, 7),
            //        Duration = new TimeSpan(1, 0, 0, 0),
            //        Progress = 20d
            //    });

            //task[0].Child.Add(
            //    new TaskDetails
            //    {
            //        TaskId = 4,
            //        TaskName = "Secure executive sponsorship",
            //        StartDate = new DateTime(2021, 1, 10),
            //        FinishDate = new DateTime(2021, 1, 14),
            //        Duration = new TimeSpan(1, 0, 0, 0),
            //        Progress = 20d
            //    });

            //task[0].Child.Add(
            //    new TaskDetails
            //    {
            //        TaskId = 5,
            //        TaskName = "Secure complete",
            //        StartDate = new DateTime(2021, 1, 14),
            //        FinishDate = new DateTime(2021, 1, 14),
            //        Duration = new TimeSpan(1, 0, 0, 0),
            //        Progress = 20d
            //    });

            return task;
        }

        public bool AddTasks(SoA_FHIR argSoA_FHIR)
        {
            int id = 1;
            foreach(PlanDefinition task in argSoA_FHIR.VisitsList)
            {
                if (task.Action.Count > 0 && task.Action[0].RelatedAction.Count > 0)
                {
                    int daysOffset = (int)((Duration)task.Action[0].RelatedAction[0].Offset).Value;
                    DateTime start = DateTime.Now.AddDays(daysOffset);
                    DateTime end = DateTime.Now.AddDays(daysOffset + 1);
                    TaskDetails newTask = new TaskDetails
                    {
                        TaskId = id++,
                        TaskName = task.Title,
                        StartDate = start,
                        FinishDate = end,
                        Progress = 1d
                    };
                     this.TaskCollection[0].Child.Add(newTask);
                    id = AddSubTasks(task.Action,newTask);
                }
            }
            return true;

        }

        private int AddSubTasks( List<PlanDefinition.ActionComponent> argActions, TaskDetails argTask)
        {
            int id = argTask.TaskId + 1;
            foreach( PlanDefinition.ActionComponent action in argActions)
            {
                TaskDetails newTask = new TaskDetails
                {
                    TaskId = id++,
                    TaskName = action.Title,
                    StartDate = argTask.StartDate,
                    FinishDate = argTask.FinishDate,
                    Progress = 1d
                };
                argTask.Child.Add(newTask);
            }

            return id;
        }
    }
}
