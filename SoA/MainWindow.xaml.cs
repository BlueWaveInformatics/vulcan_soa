﻿
// Uses Synfusion Gantt chart package  (Nuget Syncfusion.Gantt.Wpf)


using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using BW_Events;
using SoA.FHIR;
using Syncfusion.SfSkinManager;

namespace SoA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields
        private string currentVisualStyle;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the current visual style.
        /// </summary>
        /// <value></value>
        /// <remarks></remarks>
        public string CurrentVisualStyle
        {
            get
            {
                return currentVisualStyle;
            }
            set
            {
                currentVisualStyle = value;
                OnVisualStyleChanged();
            }
        }
        #endregion

        public Study Study_01 = null;
        Binding StudyIDBinding = new Binding("StudyID");
        Binding StudyNameBinding = new Binding("StudyName");

        public Spreadsheet_File Spreadsheet_01 = null;
        public EventSheet EventSheet_01 = null;
        public ODM_File ODM_File_01 = null;
        public Gantt_ViewModel Gantt_ViewModel = null;
        public MainWindow()
        {
            InitializeComponent();

            Study_01 = Study.New(Study_01);
            this.Loaded += OnLoaded;
            Gantt_ViewModel = new Gantt_ViewModel();

            tb_ResourceURL.Text = Properties.Settings.Default.SavedURL;
        }

        /// <summary>
        /// Called when [loaded].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            CurrentVisualStyle = "Metro";
        }
        /// <summary>
        /// On Visual Style Changed.
        /// </summary>
        /// <remarks></remarks>
        private void OnVisualStyleChanged()
        {
            VisualStyles visualStyle = VisualStyles.Default;
            Enum.TryParse(CurrentVisualStyle, out visualStyle);
            if (visualStyle != VisualStyles.Default)
            {
                SfSkinManager.ApplyStylesOnApplication = true;
                SfSkinManager.SetVisualStyle(this, visualStyle);
                SfSkinManager.ApplyStylesOnApplication = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            StudyIDBinding.Source = Study_01;
            tb_StudyID.SetBinding(TextBox.TextProperty, StudyIDBinding);
            StudyNameBinding.Source = Study_01;
            tb_StudyName.SetBinding(TextBox.TextProperty, StudyNameBinding);

        }

        private void btn_Spreadsheet_Click(object sender, RoutedEventArgs e)
        {
            Study_01 = Study.New(Study_01);
            
            Spreadsheet_01 = Spreadsheet_File.New();
            lb_Visits.ItemsSource = Spreadsheet_01.EventDefNameList;
            lb_Activities.ItemsSource = Spreadsheet_01.ActivityDefNameList;
            //lb_Forms.ItemsSource = Spreadsheet_01.FormDefNameList;
            //lb_Items.ItemsSource = Spreadsheet_01.ItemDefNameList;
            dg_EventSheet.ItemsSource = Spreadsheet_01.EventSheet.Rows;
            Spreadsheet_01.LoadSpreadsheet(Study_01, null);

            EventSheet_01 = Spreadsheet_01.EventSheet;

        }

        private void btn_ODM_Click(object sender, RoutedEventArgs e)
        {
            Study_01 = Study.New( Study_01);

            ODM_File_01 = ODM_File.New();
            lb_Visits.ItemsSource = ODM_File_01.EventDefNameList;
            lb_Activities.ItemsSource = ODM_File_01.ActivityDefNameList;
            lb_Forms.ItemsSource = ODM_File_01.FormDefNameList;
            lb_Items.ItemsSource = ODM_File_01.ItemDefNameList;
            dg_EventSheet.ItemsSource = ODM_File_01.EventSheet.Rows;
            ODM_File_01.LoadODMFile(Study_01, null);
            EventSheet_01 = ODM_File_01.EventSheet;
        }

        private void dgbtn_Save_Click(object sender, RoutedEventArgs e)
        {
            EventRow row = (sender as Button).DataContext as EventRow;
            SoA_FHIR soA_FHIR = new SoA_FHIR();

            string id = soA_FHIR.WriteActivityString(row.ID, row.StartDate, row.EndDate, row.Name, row.DependsOn);
            (sender as Button).Content = id;

        }

        private void btn_WriteActivities_Click(object sender, RoutedEventArgs e)
        {
            SoA_FHIR soA_FHIR = new SoA_FHIR();

            soA_FHIR.WriteAllActivities(EventSheet_01);
            soA_FHIR.WriteVisitBase(EventSheet_01);
            soA_FHIR.WriteAllVisits(EventSheet_01);
            string researchStudyID = soA_FHIR.WriteResearchStudy(Study_01, EventSheet_01);
            tb_ResourceURL.Text = researchStudyID;
            Properties.Settings.Default.SavedURL = researchStudyID;
            Properties.Settings.Default.Save();

            MessageBox.Show("Activities written");
        }

        private void btn_ReadURL_Click(object sender, RoutedEventArgs e)
        {
            SoA_FHIR soA_FHIR = new SoA_FHIR();

            lbl_StudyName.Content = soA_FHIR.LoadResearchStudy(tb_ResourceURL.Text);
            soA_FHIR.LoadVisits();
            Gantt_ViewModel.AddTasks(soA_FHIR);
            Gantt.ItemsSource = Gantt_ViewModel.TaskCollection;

        }
    }
}
